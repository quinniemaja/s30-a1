const express = require ('express');

const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json())
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://admin:admin131@bootcamp.y9mz6.mongodb.net/session30?retryWrites=true&w=majority', 
	{

		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Conncection Error"))
	db.once('open', () => console.log('Connected to the cloud database'));

const userSchema = new mongoose.Schema({

	username: String,
	password: String 
});

const Users = mongoose.model('Users', userSchema);

app.post('/signup', (req, res) => {

	Users.findOne(
		{
			username:req.body.username,
			password: req.body.password
		}, 
		(err, result) => {

		if(result != null && result.username === req.body.username) {
			return res.send('Account Already Exists.')

		} else {

			let newUser = new Users( {

				username:req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedUser) => {

				if(saveErr) {
					return console.log(saveErr)

				} else {
					return res.status(201).send('Account was successfully created!')
				}
			})
		}
	})
});

app.get('/users', (req, res) => {

	Users.find({}, (err, result) => {
		if(err) {
			return console.log(err)

		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
});




app.listen(port, () => console.log(`Server is running at port: ${port}. `));